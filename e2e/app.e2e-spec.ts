import { CasuloglobalPage } from './app.po';

describe('casuloglobal App', function() {
  let page: CasuloglobalPage;

  beforeEach(() => {
    page = new CasuloglobalPage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
