import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'mv-pagamento-form',
  templateUrl: './pagamento-form.component.html',
  styleUrls: ['./pagamento-form.component.scss']
})
export class PagamentoFormComponent implements OnInit {

  idTransacao:string;

  constructor() { }

  ngOnInit() {
    let id = this.getGenerateId(100000000,999999999);
    let unifier = this.getGenerateId(100,200);
    let sum = id + unifier;
    this.idTransacao = `CG012016${sum}`;
  }

  onSubmit(event: any, form: any) {
    form.submit();
  }

  getGenerateId(min, max) {
    let base = Math.random() * (max - min) + min
    return Math.ceil(base);
  }
}
