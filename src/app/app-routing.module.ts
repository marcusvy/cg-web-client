import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LocationStrategy, HashLocationStrategy } from '@angular/common';

import { HomeComponent } from './home/home.component';
import { CursoComponent } from './curso/curso.component';

const routes: Routes = [
  {path: '', component: HomeComponent},
  {path: 'curso', component: CursoComponent},
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
  providers: [{provide: LocationStrategy, useClass: HashLocationStrategy }]
})
export class CasuloglobalRoutingModule { }
