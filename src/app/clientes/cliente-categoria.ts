export interface IClienteCategoria {
  codigo: number;
  nome: string;
}

export class ClienteCategoria implements IClienteCategoria {

  constructor(public codigo: number, public nome: string) {
    
  }
}
