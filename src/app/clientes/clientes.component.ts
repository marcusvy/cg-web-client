import { Component, OnInit } from '@angular/core';
import { ICliente, Cliente } from './cliente';
import { IClienteCategoria, ClienteCategoria } from './cliente-categoria';

@Component({
  selector: 'mv-clientes',
  templateUrl: './clientes.component.html',
  styleUrls: ['./clientes.component.scss']
})
export class ClientesComponent implements OnInit {
  private clientes : ICliente[] = <ICliente[]>[];
  private categorias: IClienteCategoria[] = <IClienteCategoria[]>[];
  private categoriaSelecionada: number = 0;

  constructor() { }

  /**
   * Cria as categorias de clientes
   * @return {ClienteCategoria[]}
   */
  criaClienteCategoria() {
    this.categorias = [
      new ClienteCategoria(1, 'Pedagógicos'),
      new ClienteCategoria(2, 'Marketing'),
      new ClienteCategoria(3, 'Motivação'),
      new ClienteCategoria(4, 'Recursos Humanos'),
    ];
  }

  /**
   * Obtem todos os clientes
   * @return {ICliente[]} Lista de Clientes
   */
  getClientes() {
    this.clientes = [
      new Cliente("Faculdade São Lucas","",[1]),
      new Cliente("Faculdade Interamericana de Porto Velho","UNIRON", [1,2,3,4]),
      new Cliente("Faculdade de Rolim de Moura","FAROL", [1,2,3,4]),
      new Cliente("Faculdade de Pimenta Bueno","FAP", [1]),
      new Cliente("Escola Terra Nova","", [1]),
      new Cliente("Instituto Maria Auxiliadora","", [1]),
      new Cliente("Prefeitura Municipal de Porto Velho","", [1,2,3,4]),
      new Cliente("Prefeitura Municipal de Candeias do Jamari", "", [2,3,4]),
      new Cliente("Secretaria Municipal de Educação de Porto Velho","SEMED", [1]),
      new Cliente("Serviço Nacional de Aprendizagem Comercial","SENAC/RO", [1,2,3,4]),
      new Cliente("Serviço Nacional de Aprendizagem Comercial","SENAC/AC", [2,3,4]),
      new Cliente("Serviço Social da Indústria","SESI/RO", [1]),
      new Cliente("Serviço Brasileiro de Apoio às Micro e Pequenas Empresas","SEBRAE/RO", [1]),
      new Cliente("Secretaria de Estado da Educação","SEDUC/RO", [1]),
      new Cliente("Secretaria de Estado da Fazenda","SEFIN/RO", [1]),
      new Cliente("Tribunal Regional Eleitoral","TRE/RO", [1,2,3,4]),
      new Cliente("Tribunal de Justiça do Estado de Rondônia","TJ/RO", [1]),
      new Cliente("Tribunal de Justiça do Estado do Acre","TJ/AC", [1]),
      new Cliente("Departamento Estadual de Trânsito de Rondônia","DETRAN/RO", [1]),
      new Cliente("Fundação Universidade Federal de Rondônia","UNIR", [1]),
      new Cliente("Grupo Rovema","", [2,3,4]),
      new Cliente("LF Imports","", [2,3,4]),
      new Cliente("LF Seguros","", [2,3,4]),
      new Cliente("Grupo MILLA","", [2,3,4]),
      new Cliente("Grurpo AMERON","", [2,3,4]),
      new Cliente("Banco do Brasil S/A","BB", [2,3,4]),
      new Cliente("Banco do Estado de São Paulo", "BANESPA", [2,3,4]),
      new Cliente("Banco do Estado de São Paulo", "BANESPA", [2,3,4]),
      new Cliente("Brasil Telecom GSM", "", [2,3,4]),
      new Cliente("Unimed", "UNIMED/RO", [2,3,4]),
      new Cliente("Caixa Econômica Federal", "CAIXA/AC", [2,3,4]),
      new Cliente("Caixa Econômica Federal", "CAIXA/RO", [2,3,4]),
      new Cliente("Centrais Elétricas de Rondônia", "CERON", [2,3,4]),
      new Cliente("Eletronorte do Brasil", "", [2,3,4]),
      new Cliente("Empresa Brasileira de Pesquisa Agropecuária", "EMBRAPA/RO", [2,3,4]),
      new Cliente("Procuradoria Geral da República-RO", "MP/RO PGR", [2,3,4]),
      new Cliente("Ministério Público do Estado de Rondônia", "MP/RO", [2,3,4]),
      new Cliente("Gráfica e Editora Leonora", "", [2,3,4]),
      new Cliente("Guascor do Brasil", "", [2,3,4]),
      new Cliente("Norte Frios", "", [2,3,4]),
      new Cliente("Ordem dos Advogados do Brasil", "OAB/RO", [2,3,4]),
      new Cliente("Faculdades de Ciências e Letras de Rondônia", "FARO", [2,3,4]),
      new Cliente("Fundação de Assistência Social do Estado de Rondônia", "FASER", [2,3,4]),
      new Cliente("Conselho de Justiça Federal", "CNJ", [2,3,4]),
      new Cliente("TV Rondônia", "", [2,3,4])
    ];
    return this.clientes;
  }

  ngOnInit() {
    this.criaClienteCategoria();
    this.getClientes();
  }

  temCategoriaSelecionada(){
    return this.categoriaSelecionada !== 0;
  }

  isSelected(categoria: number) {
    let classes  = {
      'mv-clientes__btn--active': this.categoriaSelecionada == categoria
    };
    return classes;
  }

  selecionaCategoria(categoriaEscolhida:number) {
    this.categoriaSelecionada = categoriaEscolhida;
  }

}
