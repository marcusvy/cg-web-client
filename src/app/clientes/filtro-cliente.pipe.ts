import { Pipe, PipeTransform } from '@angular/core';
import { ICliente } from './cliente';

@Pipe({
  name: 'filtroCliente'
})
export class FiltroClientePipe implements PipeTransform {

  transform(value: any, categoria?: any): any {
    if (value.length === 0 || categoria === 0) {
      return [];
    }
    return value
    .filter(
      (cliente: any) => cliente.categoria.indexOf(categoria) >= 0
    )
    .sort((clienteA,clienteB)=>{
      // a > b = 1
      if(clienteA.nome > clienteB.nome){
        return 1;
      }
      if(clienteA.nome < clienteB.nome){
        return -1;
      }
      // a < b = -1
      return 0;
    });
  }
}
