import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'mv-cliente',
  templateUrl: './cliente.component.html',
  styleUrls: ['./cliente.component.scss']
})
export class ClienteComponent implements OnInit {
  @Input() nome: string;
  @Input() sigla: string;

  constructor() { }

  ngOnInit() {
  }

}
