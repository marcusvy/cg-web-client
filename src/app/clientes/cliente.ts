import { IClienteCategoria } from './cliente-categoria';

export interface ICliente {
  nome: string;
  sigla: string;
  categoria: number[];
}


export class Cliente implements ICliente {

  constructor (
    public nome: string = "",
    public sigla: string = "",
    public categoria: number[] = <number[]>[]
  ) {

  }
}
