import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';

import { AppComponent } from './app.component';
import { CasuloglobalRoutingModule } from './app-routing.module';
import { HeaderComponent } from './header/header.component';
import { FooterComponent } from './footer/footer.component';
import { MegaSlideComponent } from './mega-slide/mega-slide.component';
import { ParceirosComponent } from './parceiros/parceiros.component';
import { ClientesComponent } from './clientes/clientes.component';
import { ServicosComponent } from './servicos/servicos.component';
import { ClienteComponent } from './clientes/cliente/cliente.component';
import { FiltroClientePipe } from './clientes/filtro-cliente.pipe';
import { CursoComponent } from './curso/curso.component';
import { HomeComponent } from './home/home.component';
import { PagamentoFormComponent } from './curso/pagamento-form/pagamento-form.component';

@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    FooterComponent,
    MegaSlideComponent,
    ParceirosComponent,
    ClientesComponent,
    ServicosComponent,
    ClienteComponent,
    FiltroClientePipe,
    CursoComponent,
    HomeComponent,
    PagamentoFormComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule,
    CasuloglobalRoutingModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
